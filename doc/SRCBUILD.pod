=head1 NAME

SRCBUILD - Source Package Manager build recipe

=head1 DESCRIPTION

SRCBUILD is used as a script, a set of instructions, how to build software
using Source Package Manager. Its syntax is Shell (Bash), similar to those
used by Pacman's PKGBUILDs and Gentoo's ebuilds. Common things with the
PKGBUILD format are the version, description, depends, makedepends,
checkdepends, source, options and backup variables. Common things with the
ebuild format is presence of pre/post actions in the SRCBUILD itself. This means
that everything is in one file, unlike the PKGBUILD format.

The metadata defined in the recipes should contain only one function
(src_install) and two (2) variables (version and description). Optionally
two (2) more functions can be specified (src_compile and src_check) and
five (6) more variable (depends, makedepends, checkdependssource, options and
backup). Target name is defined by the directory name in which the target files
are placed in the repository.

In addition six (6) more functions (pre/post_install, pre/post_upgrade and
pre/post_remove) can be defined as pre/post actions to be taken when
software is installed, upgraded or removed. Pre-actions are taken before
the actual install/upgrade/remove is done. Post-actions are taken after the
actual install/upgrade/remove is done.

For easy of use SOURCE_DIR and INSTALL_DIR are exported and can be used
inside the SRCBUILD.

=head1 OPTIONS

=head2 version

Variable defining version of software.

=head2 description

Variable defining description of software.

=head2 depends

Variable defining runtime dependencies of software.

=head2 makedepends

Variable defining build dependencies of software.

=head2 checkdepends

Variable defining check dependencies of software.

=head2 sources

Variable defining sources of software. This includes remote files, such as
tarballs or patches, that should be fetched. Note that local files can be
shipped along the SRCBUILD when needed.

=head2 backup

Variable defining additional files to be backed up when merging software.
Note that all files with .conf extension are automatically backed up.

=head2 options

Variable defining options to be used when building software. It is used
to override the install options specified in the SPM configuration file.
Valid options are:

=head3 binaries

Strip binary files.

=head3 shared

Strip shared library files.

=head3 static

Strip static library files.

=head3 man

Compress manual page files.

=head3 mirror

Use mirrors.

=head3 missing

Ignore missing runtime dependencies.

Note that preceding an option with exclamation mark (!) has the opposite
affect.

=head3 rpath

Fix RPATH in binaries and libraries

=head2 src_compile

Function defining the instructions to compile the software.

=head2 src_check

Function defining the instructions to check (test) the software.

=head2 src_install

Function defining the instructions to install the software. Note that the
software should be installed in the temporary directory INSTALL_DIR using
DESTDIR or other method depending on the software.

=head2 pre_install

Function defining instructions what to do before installing the target.

=head2 post_install

Function defining instructions what to do after installing the target.

=head2 pre_upgrade

Function defining instructions what to do before upgrading the target.

=head2 post_upgrade

Function defining instructions what to do after upgrading the target.

=head2 pre_remove

Function defining instructions what to do before removing the target.

=head2 post_remove

Function defining instructions what to do after removing the target.

=head1 EXAMPLES

A prototype SRCBUILD should be installed in #SHAREDIR#/spm.

=head1 AUTHORS

Ivailo Monev (a.k.a. SmiL3y) <xakepa10@gmail.com>

Copyright (c) 2013-2014 Ivailo Monev licensed through the GNU General Public License

=head1 SEE ALSO

spm(8) spm.conf(5) bash(1)
