VERSION = 2.2.7
GIT_VERSION = $(shell git rev-parse --short HEAD || echo stable)

DESTDIR = 
BINDIR = /bin
SHAREDIR = /usr/share
MANDIR = $(SHAREDIR)/man
SITEDIR = /usr/lib/python$(shell python2 -c "import sys; print sys.version[:3]")/site-packages

MACHINE = $(shell gcc -dumpmachine)
ARCH = $(shell uname -m | sed 's|_|-|')
JOBS = $(shell echo $$((`nproc`+1)))

NUITKA = python2 ../../nuitka/bin/nuitka

# for use with --preserve-context if applicable
INSTALL = install

all: main libspm tools man

man: clean-doc
	pod2man --release="SPM $(VERSION)" --center="Source Package Manager Manual" \
		--section='8' --utf8 doc/spm.pod doc/spm.8
	pod2man --release="SPM $(VERSION)" --center="Source Package Manager Manual" \
		--section='5' --utf8 doc/spm.conf.pod doc/spm.conf.5
	pod2man --release="SPM $(VERSION)" --center="Source Package Manager Manual" \
		--section='5' --utf8 doc/SRCBUILD.pod doc/SRCBUILD.5
	pod2man --release="SPM $(VERSION)" --center="Source Package Manager Manual" \
		--section='8' --utf8 doc/spm-tools.pod > doc/spm-tools.8
	sed 's|#SHAREDIR#|$(SHAREDIR)|g' -i doc/SRCBUILD.5
	sed 's|#MACHINE#|$(MACHINE)|g' -i doc/spm.conf.5
	sed 's|#ARCH#|$(ARCH)|g' -i doc/spm.conf.5
	sed 's|#JOBS#|$(JOBS)|g' -i doc/spm.conf.5

# for debugging: --python-debug --trace-execution --debug
# NOTE: as of Nuitka v5.0.0 "--exe" will become default and "--module" will have
# to be passed for libspm build, "--exe" can be omited from main, qt and tools

main: bump clean-build-main
	mkdir -p build/main
	cd build/main && $(NUITKA) --exe --recurse-not-to=libspm --recurse-all \
		--code-gen-no-statement-lines --show-progress ../../src/main.py

qt: clean-build-qt
	mkdir -p build/qt
	pyuic4 src/qt.ui -o src/lib/qt4.py
	cd build/qt && $(NUITKA) --exe --recurse-not-to=libspm --recurse-all \
		--code-gen-no-statement-lines --show-progress ../../src/qt.py

tools: bump clean-build-tools
	mkdir -p build/tools
	cd build/tools && $(NUITKA) --exe --recurse-not-to=libspm --recurse-all \
		--code-gen-no-statement-lines --show-progress ../../src/tools.py

libspm: clean-build-libspm
	mkdir -p build/libspm
	cd build/libspm && $(NUITKA) --recurse-all --show-progress ../../src/libspm.py

check:
	cd src && python2 test.py
	cd scripts && bash -O extglob -n *.sh

install-doc:
	$(INSTALL) -vdm755 $(DESTDIR)$(MANDIR)/man8/ $(DESTDIR)$(MANDIR)/man5/ \
		$(DESTDIR)$(SHAREDIR)/spm/
	$(INSTALL) -vm644 doc/spm.8 $(DESTDIR)$(MANDIR)/man8/spm.8
	$(INSTALL) -vm644 doc/spm.conf.5 $(DESTDIR)$(MANDIR)/man5/spm.conf.5
	$(INSTALL) -vm644 doc/SRCBUILD.5 $(DESTDIR)$(MANDIR)/man5/SRCBUILD.5
	$(INSTALL) -vm644 doc/SRCBUILD.txt $(DESTDIR)$(SHAREDIR)/spm/SRCBUILD.txt
	$(INSTALL) -vm644 doc/spm-tools.8 $(DESTDIR)$(MANDIR)/man8/spm-tools.8

install-etc:
	$(INSTALL) -vdm755 $(DESTDIR)/etc/spm
	$(INSTALL) -vm644 etc/spm.conf $(DESTDIR)/etc/
	$(INSTALL) -vm644 etc/repositories.conf $(DESTDIR)/etc/spm/
	$(INSTALL) -vm644 etc/mirrors.conf $(DESTDIR)/etc/spm/
	sed 's|#MACHINE#|$(MACHINE)|g' -i $(DESTDIR)/etc/spm.conf
	sed 's|#ARCH#|$(ARCH)|g' -i $(DESTDIR)/etc/spm.conf
	sed 's|#JOBS#|$(JOBS)|g' -i $(DESTDIR)/etc/spm.conf

install-misc:
	$(INSTALL) -vdm755 $(DESTDIR)/etc/bash_completion.d $(DESTDIR)/etc/logrotate.d
	$(INSTALL) -vm644 misc/spm.bash $(DESTDIR)/etc/bash_completion.d/spm
	$(INSTALL) -vm644 misc/spm-tools.bash $(DESTDIR)/etc/bash_completion.d/spm-tools
	$(INSTALL) -vm644 misc/spm.logrotate $(DESTDIR)/etc/logrotate.d/spm

install-scripts:
	$(INSTALL) -vdm755 $(DESTDIR)$(BINDIR)
	$(INSTALL) -vm755 scripts/pkg2src.sh $(DESTDIR)$(BINDIR)/pkg2src
	$(INSTALL) -vm755 scripts/srcmake.sh $(DESTDIR)$(BINDIR)/srcmake

install-main:
	$(INSTALL) -vdm755 $(DESTDIR)$(BINDIR)
	$(INSTALL) -vm755 build/main/main.exe $(DESTDIR)$(BINDIR)/spm

install-qt:
	$(INSTALL) -vdm755 $(DESTDIR)$(BINDIR) $(DESTDIR)$(SHAREDIR)/applications
	$(INSTALL) -vm755 build/qt/qt.exe $(DESTDIR)$(BINDIR)/spm-qt
	$(INSTALL) -vm644 misc/spm-qt.desktop $(DESTDIR)$(SHAREDIR)/applications/spm-qt.desktop

install-tools:
	$(INSTALL) -vdm755 $(DESTDIR)$(BINDIR)
	$(INSTALL) -vm755 build/tools/tools.exe $(DESTDIR)$(BINDIR)/spm-tools

install-libspm:
	$(INSTALL) -vdm755 $(DESTDIR)$(SITEDIR)
	$(INSTALL) -vm755 build/libspm/libspm.so $(DESTDIR)$(SITEDIR)/libspm.so

install: install-etc install-doc install-scripts install-main install-libspm install-tools

uninstall:
	$(RM) -r $(DESTDIR)$(SHAREDIR)/spm/
	$(RM) $(DESTDIR)$(BINDIR)/pkg2src
	$(RM) $(DESTDIR)$(BINDIR)/srcmake
	$(RM) $(DESTDIR)$(BINDIR)/spm
	$(RM) $(DESTDIR)/etc/spm.conf
	$(RM) -r $(DESTDIR)/etc/spm
	
	$(RM) $(DESTDIR)$(MANDIR)/man8/spm.8
	$(RM) $(DESTDIR)$(MANDIR)/man5/spm.conf.5
	$(RM) $(DESTDIR)$(MANDIR)/man5/SRCBUILD.5
	$(RM) $(DESTDIR)$(MANDIR)/man8/spm-tools.8
	
	$(RM) $(DESTDIR)/etc/bash_completion.d/spm
	$(RM) $(DESTDIR)$(SHAREDIR)/applications/spm-qt.desktop
	
	$(RM) $(DESTDIR)$(SITEDIR)/libspm.so

bump:
	sed 's|^app_version.*|app_version = "$(VERSION) ($(GIT_VERSION))"|' -i src/main.py src/tools.py

changelog:
	git log HEAD -n 1 --pretty='%cd %an <%ae> %n%H%d'
	git log 2.2.6..HEAD --no-merges --pretty='    * %s'

dist: clean-dist
	git archive HEAD --prefix=spm-$(VERSION)/ | xz > spm-$(VERSION).tar.xz

stat:
	cloc $(shell find src/ -name '*.py') scripts/*.sh

lint:
	cd src && pylint lib/*.py mode/*.py main.py qt.py tools.py libspm.py | grep -v -e 'Line too long'

clean-build: clean-build-main clean-build-qt clean-build-tools clean-build-libspm

clean-build-main:
	$(RM) -r build/main
	$(RM) $(shell find . -name '*.pyc' -o -name '*.pyo')

clean-build-qt:
	$(RM) -r build/qt
	$(RM) $(shell find . -name '*.pyc' -o -name '*.pyo')

clean-build-tools:
	$(RM) -r build/tools
	$(RM) $(shell find . -name '*.pyc' -o -name '*.pyo')

clean-build-libspm:
	$(RM) -r build/libspm
	$(RM) $(shell find . -name '*.pyc' -o -name '*.pyo')

clean-doc:
	$(RM) doc/*.8 doc/*.5

clean-dist:
	$(RM) *.tar.xz

clean: clean-build clean-doc clean-dist

.PHONY: all bump changelog main qt tools libspm man check install-doc install-etc \
	install-misc install-scripts install-main install-qt install-tools install-libspm \
	install uninstall dist clean-build clean-build-main clean-build-qt clean-build-tools \
	clean-build-libspm clean-doc clean-dist clean
