#!/usr/bin/python2

import os
import subprocess
from datetime import datetime

from libspm import config
from libspm import message
from libspm import misc
from libspm import database


class Repo(object):
    ''' Class for dealing with repositories '''
    def __init__(self, repositories_urls, do_clean=False, do_sync=False, do_update=False):
        self.repositories_urls = repositories_urls
        self.do_clean = do_clean
        self.do_sync = do_sync
        self.do_update = do_update

    def clean(self):
        ''' Clean repository '''
        if os.path.isdir(self.repository_dir):
            message.sub_info('Removing', self.repository_dir)
            misc.dir_remove(self.repository_dir)
        else:
            message.sub_debug('Dirctory is OK', self.repository_dir)

    def sync(self):
        ''' Sync repository '''
        rdir = os.path.join(config.CACHE_DIR, 'repositories')
        if not os.path.isdir(rdir):
            os.makedirs(rdir)

        if os.path.isdir(self.repository_dir):
            message.sub_info('Updating repository', self.repository_name)
            subprocess.check_call((misc.whereis('git'), 'pull', '--depth=1', self.repository_url),
                cwd=self.repository_dir)
        else:
            message.sub_info('Cloning initial copy', self.repository_name)
            subprocess.check_call((misc.whereis('git'), 'clone', '--depth=1', self.repository_url,
                self.repository_dir))

    def update(self):
        ''' Check repositories for updates '''
        message.sub_info('Checking for updates')
        for target in database.local_all(basename=True):
            target_dir = database.remote_search(target)
            if not target_dir:
                message.sub_warning('Target not in any repository', target)
                continue

            message.sub_debug('Checking', target)
            local_version = database.local_metadata(target, 'version')
            remote_version = database.remote_metadata(target, 'version')

            if misc.version(local_version) < misc.version(remote_version):
                message.sub_warning('New version of %s available' % target, remote_version)

    def main(self):
        ''' Execute action for every repository '''
        for repository in self.repositories_urls:
            self.repository_url = repository
            self.repository_name = os.path.basename(self.repository_url).replace('.git', '')
            self.repository_dir = os.path.join(config.CACHE_DIR, 'repositories', self.repository_name)

            if self.do_clean:
                message.sub_info('Starting cleanup at', datetime.today())
                self.clean()

            if self.do_sync:
                message.sub_info('Starting sync at', datetime.today())
                self.sync()

        if self.do_update:
            message.sub_info('Starting update at', datetime.today())
            self.update()
