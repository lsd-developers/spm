#!/usr/bin/python2

import re

from libspm import message
from libspm import database
from libspm import misc


class Local(object):
    ''' Class for printing local targets metadata '''
    def __init__(self, pattern, do_name=False, do_version=False, do_description=False,
        do_depends=False, do_reverse=False, do_size=False, do_footprint=False,
        plain=False):
        self.pattern = pattern
        self.do_name = do_name
        self.do_version = do_version
        self.do_description = do_description
        self.do_depends = do_depends
        self.do_reverse = do_reverse
        self.do_size = do_size
        self.do_footprint = do_footprint
        self.plain = plain

    def main(self):
        ''' Print local target metadata for every match '''
        for target in database.local_all(basename=True):
            if re.search(self.pattern, target):
                if self.do_name and self.plain:
                    print(target)
                elif self.do_name:
                    message.sub_info('Name', target)

                if self.do_version and self.plain:
                    print(database.local_metadata(target, 'version'))
                elif self.do_version:
                    message.sub_info('Version', database.local_metadata(target, 'version'))

                if self.do_description and self.plain:
                    print(database.local_metadata(target, 'description'))
                elif self.do_description:
                    message.sub_info('Description', database.local_metadata(target, 'description'))

                if self.do_depends and self.plain:
                    print(misc.string_convert(database.local_metadata(target, 'depends')))
                elif self.do_depends:
                    message.sub_info('Depends', database.local_metadata(target, 'depends'))

                if self.do_reverse and self.plain:
                    print(misc.string_convert(database.local_rdepends(target)))
                elif self.do_reverse:
                    message.sub_info('Reverse depends', database.local_rdepends(target))

                if self.do_size and self.plain:
                    print(database.local_metadata(target, 'size'))
                elif self.do_size:
                    message.sub_info('Size', database.local_metadata(target, 'size'))

                if self.do_footprint and self.plain:
                    print(database.local_footprint(target))
                elif self.do_footprint:
                    message.sub_info('Footprint', database.local_footprint(target))
