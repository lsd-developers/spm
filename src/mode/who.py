#!/usr/bin/python2

from libspm import message
from libspm import database


class Who(object):
    ''' Class for printing file owner '''
    def __init__(self, pattern, plain=False):
        self.pattern = pattern
        self.plain = plain

    def main(self):
        ''' Print owner of match '''
        for target in database.local_belongs(self.pattern, escape=False):
            if self.plain:
                print(target)
            else:
                message.sub_info('Match in', target)
