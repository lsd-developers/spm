#!/usr/bin/python2

import re

from libspm import message
from libspm import database
from libspm import misc


class Remote(object):
    ''' Class for printing remote targets metadata '''
    def __init__(self, pattern, do_name=False, do_version=False, do_description=False,
        do_depends=False, do_makedepends=False, do_checkdepends=False, do_sources=False,
        do_options=False, do_backup=False, plain=False):
        self.pattern = pattern
        self.do_name = do_name
        self.do_version = do_version
        self.do_description = do_description
        self.do_depends = do_depends
        self.do_makedepends = do_makedepends
        self.do_checkdepends = do_checkdepends
        self.do_sources = do_sources
        self.do_options = do_options
        self.do_backup = do_backup
        self.plain = plain

    def main(self):
        ''' Print remote target metadata for every match '''
        for target in database.remote_all(basename=True):
            if re.search(self.pattern, target):
                if self.do_name and self.plain:
                    print(target)
                elif self.do_name:
                    message.sub_info('Name', target)

                if self.do_version and self.plain:
                    print(database.remote_metadata(target, 'version'))
                elif self.do_version:
                    message.sub_info('Version', database.remote_metadata(target, 'version'))

                if self.do_description and self.plain:
                    print(database.remote_metadata(target, 'description'))
                elif self.do_description:
                    message.sub_info('Description', database.remote_metadata(target, 'description'))

                if self.do_depends and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'depends')))
                elif self.do_depends:
                    message.sub_info('Depends', database.remote_metadata(target, 'depends'))

                if self.do_makedepends and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'makedepends')))
                elif self.do_makedepends:
                    message.sub_info('Make depends', database.remote_metadata(target, 'makedepends'))

                if self.do_checkdepends and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'checkdepends')))
                elif self.do_checkdepends:
                    message.sub_info('Check depends', database.remote_metadata(target, 'checkdepends'))

                if self.do_sources and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'sources')))
                elif self.do_sources:
                    message.sub_info('Sources', database.remote_metadata(target, 'sources'))

                if self.do_options and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'options')))
                elif self.do_options:
                    message.sub_info('Options', database.remote_metadata(target, 'options'))

                if self.do_backup and self.plain:
                    print(misc.string_convert(database.remote_metadata(target, 'backup')))
                elif self.do_backup:
                    message.sub_info('Backup', database.remote_metadata(target, 'backup'))
