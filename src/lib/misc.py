#!/usr/bin/python2

import os
import re
import shutil
import urllib2
import tarfile
import zipfile
import subprocess
import httplib

import lib.config as config
import lib.magic as magic


def whereis(program, fallback=True):
    ''' Find full path to executable '''
    for path in os.environ.get('PATH', '/bin:/usr/bin').split(':'):
        exe = os.path.join(path, program)
        if os.path.isfile(exe):
            return exe
    if fallback:
        # if only the OSError exception was a bit more robust. in the future,
        # fallback will return program and let OSError be raised at higher level
        raise OSError('Program not found in PATH', program)
    return None


def ping(url='http://www.google.com'):
    ''' Ping URL '''
    try:
        p = urllib2.urlopen(url, timeout=config.TIMEOUT)
        p.close()
        return True
    except (urllib2.URLError, httplib.BadStatusLine):
        return False


def version(variant):
    ''' Convert input to tuple suitable for version comparison '''
    # if None is passed, e.g. on invalid remote target, the split will fail
    if not variant:
        return ()
    return tuple([int(x) for x in variant.split('.') if x.isdigit()])


def string_convert(string):
    ''' Conver input to string but only if it really is list '''
    if isinstance(string, list) or isinstance(string, tuple):
        return ' '.join(string)
    return string

def string_search(string, string2, exact=False, escape=True):
    ''' Search for string in other string or list '''
    if exact and escape:
        return re.findall('(\\s|^)' + re.escape(string) + '(\\s|$)', string_convert(string2))
    elif exact:
        return re.findall('(\\s|^)' + string + '(\\s|$)', string_convert(string2))
    elif escape:
        return re.findall(re.escape(string), string_convert(string2))
    else:
        return re.findall(string, string_convert(string2))


def file_read(sfile):
    ''' Get file content '''
    rfile = open(sfile, 'r')
    content = rfile.read()
    rfile.close()
    return content

def file_readlines(sfile):
    ''' Get file content, split by new line, as list '''
    rfile = open(sfile, 'r')
    content = rfile.read().splitlines()
    rfile.close()
    return content

def file_write(sfile, content):
    ''' Write data to file (overwrites) '''
    dirname = os.path.dirname(sfile)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    wfile = open(sfile, 'w')
    wfile.write(content)
    wfile.close()

def file_search(string, sfile, exact=False, escape=True):
    ''' Search for string in file '''
    return string_search(string, file_read(sfile), exact=exact, escape=escape)

def file_mime(sfile):
    ''' Get file type '''
    # symlinks are not handled properly by magic, on purpose
    # https://github.com/ahupp/python-magic/pull/31
    if os.path.islink(sfile):
        return 'inode/symlink'
    return magic.from_file(sfile, mime=True)


def dir_remove(sdir):
    ''' Remove directory recursively '''
    for root, dirs, files in os.walk(sdir):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            if os.path.islink(os.path.join(root, d)):
                os.unlink(os.path.join(root, d))
    for root, dirs, files in os.walk(sdir, topdown=False):
        for d in dirs:
            os.rmdir(os.path.join(root, d))
    os.rmdir(sdir)

def dir_size(sdir):
    ''' Get size of directory '''
    size = 0
    for sfile in list_files(sdir):
        if os.path.islink(sfile):
            continue
        size += os.path.getsize(sfile)
    return size


def list_files(directory):
    ''' Get list of files in directory recursively '''
    slist = []
    for root, subdirs, files in os.walk(directory):
        for sfile in files:
            slist.append(os.path.join(root, sfile))
    return slist

def list_dirs(directory):
    ''' Get list of directories in directory recursively '''
    slist = []
    for root, subdirs, files in os.walk(directory):
        for sdir in subdirs:
            slist.append(os.path.join(root, sdir))
    return slist


def fetch_check(url, destination):
    ''' Check if remote file and file sizes are equal '''
    # not all requests can get content-lenght , this means that there is no way
    # to tell if the archive is corrupted (checking if size == 0 is not enough)
    # so the source is re-feteched

    if os.path.isfile(destination):
        local_size = os.path.getsize(destination)
        rfile = urllib2.urlopen(url, timeout=config.TIMEOUT)
        remote_size = rfile.headers.get('content-length')
        rfile.close()

        if not remote_size:
            return False
        elif int(local_size) == int(remote_size):
            return True
    else:
        return False

def fetch_internal(url, destination):
    ''' Download file using internal library '''
    rfile = urllib2.urlopen(url, timeout=config.TIMEOUT)
    dest_dir = os.path.dirname(destination)

    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    output = open(destination, 'wb')
    output.write(rfile.read())
    output.close()
    rfile.close()

def fetch(url, destination):
    ''' Download file using external utilities, fallback to internal '''
    dest_dir = os.path.dirname(destination)
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    curl = whereis('curl', fallback=False)
    wget = whereis('wget', fallback=False)
    if config.EXTERNAL and curl:
        subprocess.check_call((curl, '--connect-timeout', str(config.TIMEOUT),
            '--fail', '--retry', '10', '--location', '--continue-at', '-',
            url, '--output', destination))
    elif config.EXTERNAL and wget:
        subprocess.check_call((wget, '--timeout', str(config.TIMEOUT),
            '--continue', url, '--output-document', destination))
    else:
        fetch_internal(url, destination)


def archive_compress(variant, sfile, method='bz2'):
    ''' Create archive from directory '''
    dirname = os.path.dirname(sfile)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    tar = tarfile.open(sfile, 'w:' + method)
    if isinstance(variant, list) or isinstance(variant, tuple):
        for item in variant:
            tar.add(item)
    else:
        tar.add(variant, '')
    tar.close()

def archive_decompress(sfile, sdir):
    ''' Extract archive to directory '''
    if not os.path.isdir(sdir):
        os.makedirs(sdir)

    # WARNING!!! the -P option is not supported by the
    # Busybox version of `tar`.

    # standard tarfile library locks the filesystem and upon interrupt the
    # filesystem stays locked which is bad. on top of that the tarfile library
    # can not replace files while they are being used thus the external
    # utilities are used for extracting archives.

    # alotught bsdtar can (or should) handle Zip files we do not use it for them.
    if sfile.endswith('.xz') or sfile.endswith('.lzma') or tarfile.is_tarfile(sfile):
        bsdtar = whereis('bsdtar', fallback=False)
        tar = whereis('tar')
        if bsdtar:
            subprocess.check_call((bsdtar, '-xpPf', sfile, '-C', sdir))
        else:
            subprocess.check_call((tar, '-xpPhf', sfile, '-C', sdir))
    elif zipfile.is_zipfile(sfile):
        zfile = zipfile.ZipFile(sfile, 'r')
        zfile.extractall(path=sdir)
        zfile.close()

def archive_list(sfile):
    ''' Get list of files in archive '''
    content = []
    if tarfile.is_tarfile(sfile):
        tfile = tarfile.open(sfile)
        content = tfile.getnames()
        tfile.close()
    elif zipfile.is_zipfile(sfile):
        zfile = zipfile.ZipFile(sfile)
        content = zfile.namelist()
        zfile.close()
    elif sfile.endswith('.xz') or sfile.endswith('.lzma'):
        content = system_output((whereis('tar'), '-tf', sfile)).split('\n')
    return content

def archive_size(star, sfile):
    ''' Get size of file in archive '''
    size = 0
    tar = tarfile.open(star, 'r')
    for i in tar.getmembers():
        if i.name == sfile:
            size = i.size
            break
    tar.close()
    return size


def system_output(command):
    ''' Get output of external utility '''
    pipe = subprocess.Popen(command, stdout=subprocess.PIPE, env={'LC_ALL': 'C'})
    return pipe.communicate()[0].strip()

def system_scanelf(sfile, format='#F%n'):
    ''' Get information about ELF files '''
    return system_output((whereis('scanelf'), '-CBF', format, sfile))

def system_chroot(command):
    ''' Execute command in chroot environment '''
    # prevent stupidity
    if config.ROOT_DIR == '/':
        return

    real_root = os.open('/', os.O_RDONLY)
    mount = whereis('mount')
    try:
        for s in ('/proc', '/dev', '/sys'):
            sdir = config.ROOT_DIR + s
            if not os.path.ismount(sdir):
                if not os.path.isdir(sdir):
                    os.makedirs(sdir)
                subprocess.check_call((mount, '--rbind', s, sdir))
        os.chroot(config.ROOT_DIR)
        os.chdir('/')
        subprocess.check_call(command)
    finally:
        os.fchdir(real_root)
        os.chroot('.')
        os.close(real_root)
        for s in ('/proc', '/dev', '/sys'):
            sdir = config.ROOT_DIR + s
            if os.path.ismount(sdir):
                subprocess.check_call((mount, '--force', '--lazy', sdir))

def system_script(srcbuild, function):
    ''' Execute pre/post actions '''
    if config.ROOT_DIR == '/':
        subprocess.check_call((whereis('bash'), '-e', '-c', 'source ' + srcbuild + ' && ' + function),
            cwd=config.ROOT_DIR)
    else:
        shutil.copy(srcbuild, os.path.join(config.ROOT_DIR, 'SRCBUILD'))
        system_chroot(('bash', '-e', '-c', 'source /SRCBUILD && ' + function))
        os.remove(os.path.join(config.ROOT_DIR, 'SRCBUILD'))
