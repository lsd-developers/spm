#!/usr/bin/python2

import os

import lib.config as config
import lib.misc as misc
import lib.srcbuild as srcbuild


def remote_all(basename=False):
    ''' Returns directories of all remote (repository) targets '''
    remote_list = []

    for sdir in misc.list_dirs(os.path.join(config.CACHE_DIR, 'repositories')):
        if os.path.isfile(os.path.join(sdir, 'SRCBUILD')) and basename:
            remote_list.append(os.path.basename(sdir))
        elif os.path.isfile(os.path.join(sdir, 'SRCBUILD')):
            remote_list.append(sdir)
    return sorted(remote_list)


def local_all(basename=False):
    ''' Returns directories of all local (installed) targets '''
    local_list = []

    # it may not exists if bootstrapping
    if not os.path.isdir(config.LOCAL_DIR):
        return local_list

    for sdir in misc.list_dirs(config.LOCAL_DIR):
        if os.path.isfile(os.path.join(sdir, 'metadata')) and basename:
            local_list.append(os.path.basename(sdir))
        elif os.path.isfile(os.path.join(sdir, 'metadata')):
            local_list.append(sdir)
    return sorted(local_list)


def remote_search(target):
    ''' Returns full path to directory matching target '''
    if os.path.isfile(os.path.join(target, 'SRCBUILD')):
        return target

    for rtarget in remote_all():
        if rtarget == target \
            or os.path.basename(rtarget) == os.path.basename(target):
            return rtarget
    return None


def local_installed(target):
    ''' Returns True or False wheather target is installed '''
    if not os.path.isdir(config.LOCAL_DIR):
        return False
    elif misc.string_search(os.path.basename(target),
        local_all(basename=True), exact=True):
        return True
    elif misc.string_search(target, local_all(), exact=True):
        return True
    return False


def local_belongs(sfile, exact=False, escape=True, ignore=None):
    ''' Searches for match of file in all local targets '''
    match = []

    # it may not exists if bootstrapping
    if not os.path.isdir(config.LOCAL_DIR):
        return match

    for local in local_all(basename=True):
        if local == ignore:
            continue

        if misc.string_search(sfile, local_footprint(local),
            exact=exact, escape=escape):
            match.append(local)
    return match


def remote_mdepends(target, checked=None, cdepends=False):
    ''' Returns missing build dependencies of target '''

    # depends, {make,check}depends are optional and
    # relying on them to be different than None
    # would break the code, they can not be
    # concentrated nor looped trough
    depends = remote_metadata(target, 'depends')
    makedepends = remote_metadata(target, 'makedepends')
    checkdepends = remote_metadata(target, 'checkdepends')

    missing = []
    build_depends = []
    if checked is None:
        checked = []

    # respect ignored targets to avoid building
    # dependencies when not needed
    if misc.string_search(target, config.IGNORE, exact=True):
        return missing

    if depends:
        build_depends.extend(depends)
    if makedepends:
        build_depends.extend(makedepends)
    if cdepends and checkdepends:
        build_depends.extend(checkdepends)

    for dependency in build_depends:
        if checked and misc.string_search(dependency, checked, exact=True):
            continue

        if not misc.string_search(dependency, missing, exact=True) \
            and not local_installed(dependency) \
            or not local_uptodate(dependency):
            missing.extend(remote_mdepends(dependency, checked))
            missing.append(dependency)
            checked.extend((target, dependency))
    return missing


def local_rdepends(target):
    ''' Returns reverse dependencies of target '''
    revdeps = []
    for installed in local_all(basename=True):
        if misc.string_search(os.path.basename(target),
            local_metadata(installed, 'depends'), exact=True) \
            and not misc.string_search(installed, config.IGNORE, exact=True):
            revdeps.append(installed)
    return revdeps


def local_footprint(target):
    ''' Returns files of target '''
    relative_path = os.path.join(config.LOCAL_DIR, target, 'footprint')
    full_path = os.path.join(target, 'footprint')
    if os.path.isfile(relative_path):
        return misc.file_read(relative_path)
    elif os.path.isfile(full_path):
        return misc.file_read(full_path)


def local_metadata(target, key):
    ''' Returns metadata of local target '''
    target_metadata = os.path.join(config.LOCAL_DIR, target, 'metadata')
    if os.path.isfile(target_metadata):
        # this is not optimal, but cleaner
        # for line in misc.file_readlines(target_metadata):
        #     if line.startswith(key + '='):
        #         return line.split('=')[1].strip()

        metadata_content = misc.file_read(target_metadata)
        if key == 'version':
            return metadata_content.split('\n')[0].replace('version=', '').strip()
        elif key == 'description':
            return metadata_content.split('\n')[1].replace('description=', '').strip()
        elif key == 'depends':
            return metadata_content.split('\n')[2].replace('depends=', '').strip()
        elif key == 'size':
            return metadata_content.split('\n')[3].replace('size=', '').strip()

def local_uptodate(target):
    ''' Returns True if target is up-to-date and False otherwise '''
    # if remote target is passed and it's a directory not a base name
    # then the local target will be invalid and local_version will equal
    # None, thus we use the base name to find the local target version
    local_version = local_metadata(os.path.basename(target), 'version')
    remote_version = remote_metadata(target, 'version')

    if misc.version(local_version) < misc.version(remote_version):
        return False
    return True


def remote_metadata(target, key):
    ''' Returns metadata of remote target '''
    match = remote_search(target)
    if os.path.isfile(os.path.join(target, 'SRCBUILD')):
        src = os.path.join(target, 'SRCBUILD')
    elif match:
        src = os.path.join(match, 'SRCBUILD')
    else:
        return None
    return getattr(srcbuild.SRCBUILD(src), key)


def remote_aliases(basename=True):
    ''' Returns basename of all aliases '''
    aliases = []
    for sfile in misc.list_files(os.path.join(config.CACHE_DIR, 'repositories')):
        if sfile.endswith('.alias'):
            if basename:
                aliases.append(os.path.basename(sfile.replace('.alias', '')))
            else:
                aliases.append(sfile.replace('.alias', ''))
    return sorted(aliases)


def remote_alias(target):
    ''' Returns alias for target, if not returns original '''
    for alias in remote_aliases(basename=False):
        if os.path.basename(target) == os.path.basename(alias):
            return misc.file_readlines(alias + '.alias')
    return target
